﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security;
using System.Threading.Tasks;
using System.IO;

namespace LaunchVS_SU
{
    class Program
    {
        static int Main(string[] args)
        {
            Process process = new Process();
            StreamReader initFile = new StreamReader("LaunchVS-SU.ini");
            SecureString pw = new SecureString();
            string buffer;
            int retCode = 0;

            buffer = initFile.ReadLine();
            process.StartInfo.Domain = buffer;

            buffer = initFile.ReadLine();
            process.StartInfo.UserName = buffer;

            buffer = initFile.ReadLine();

            foreach (char c in buffer.ToCharArray())
            {
                pw.AppendChar (c);
            }


            process.StartInfo.FileName = @"D:\Software\VS2013\Common7\IDE\devenv.exe";
            process.StartInfo.WorkingDirectory = @"D:\Software\VS2013\Common7\IDE";
            process.StartInfo.Arguments = "";
            process.StartInfo.Password = pw;
            process.StartInfo.UseShellExecute = false;
            process.Start();
            process.WaitForExit();
            retCode = process.ExitCode;
            initFile.Close();
            initFile.Dispose();

            return retCode;
        }
    }
}
